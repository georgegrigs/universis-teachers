import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesDetailsComponent } from './courses-details.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { MostModule } from '@themost/angular';
import { FormsModule } from '@angular/forms';
import { ConfigurationService } from '@universis/common';
import { TestingConfigurationService } from 'src/app/test';
import { ErrorService } from '@universis/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CoursesDetailsComponent', () => {
  let component: CoursesDetailsComponent;
  let fixture: ComponentFixture<CoursesDetailsComponent>;

  const errorSvc = jasmine.createSpyObj('ErrorService,', ['getLastError', 'navigateToError', 'setLastError']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        TranslateModule.forRoot(),
        FormsModule,
        MostModule.forRoot({
            base: '/',
            options: {
                useMediaTypeExtensions: false
            }
        })
    ],
      declarations: [ CoursesDetailsComponent ],
      providers: [
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        },
        {
          provide: ErrorService,
          useValue: errorSvc
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
