import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { AppSidebarService } from './../teachers-shared/services/app-sidebar.service';
import { UserService, ApplicationSettingsConfiguration } from '@universis/common';
import { ConfigurationService } from '@universis/common';
import { NavigationEnd, NavigationStart, Router, RoutesRecognized } from '@angular/router';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { ApplicationSettings } from '../teachers-shared/teachers-shared.module';
import ISO6391 from 'iso-639-1';

@Component({
  selector: 'app-layouts',
  templateUrl: './full-layout.component.html',
  styles: [`
    button.dropdown-item {
      cursor: pointer;
    }
    `]
})

export class FullLayoutComponent implements OnInit {

  constructor(private _context: AngularDataContext,
    private _appSidebar: AppSidebarService,
    private _configurationService: ConfigurationService,
    private _userService: UserService,
    private _router: Router,
    private _translateService: TranslateService,) {
    this._router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.buildLanguages(event.urlAfterRedirects.toString());
        this.navItems = this._appSidebar.navigationItems;
      }
    });
  }
  @ViewChild('appSidebarNav') appSidebarNav: any;
  public status = { isOpen: false };
  public user;
  public currentLang;
  public languages: any = [];
  public lang: any = {};
  public navItems: any = [];
  public logoPath;
  public contactUrl;

  ngOnInit(): void {
    // get current user
    this._userService.getUser().then(user => {
      this.user = user;
      // get current language
      this.currentLang = ISO6391.getNativeName(this._configurationService.currentLocale);
      // get languages
      this.languages = this._configurationService.settings.i18n.locales;
      // get path of brand logo
      this.logoPath = this._configurationService.settings.app.image;
      // get Contact Link
      this.contactUrl = this._configurationService.settings.app && (<ApplicationSettings>this._configurationService.settings.app).contactUrl;
      // build languages
      this.buildLanguages(this._router.url.toString());
      // set navigation items
      this.navItems = this._appSidebar.navigationItems;
      for (let i = 0; i < this.languages.length; i++) {
        this.languages[i] = ISO6391.getNativeName(this.languages[i]);
        this._translateService.onLangChange.subscribe((event: LangChangeEvent) => {
          window.location.reload();
        });
      }
    });
  }

  buildLanguages(url) {
    const findLang = this._appSidebar.navigationItems.find(x => {
      return x.url === '/lang';
    });
    if (findLang && findLang.children.length === 0) {
      for (let i = 0, x = 0; i < this._configurationService.settings.i18n.locales.length; i++ , x = x + 5) {
        this.lang = {};
        this.lang.name = ISO6391.getNativeName(this._configurationService.settings.i18n.locales[i]);
        this.lang.url = '/lang/'.concat(ISO6391.getCode(this.lang.name)).concat('/').concat(url);
        this.lang.icon = 'icon-cursor';
        this.lang.index = x;
        findLang.children.push(this.lang);
        if (this._configurationService.settings.i18n.locales.length < findLang.children.length) {
          findLang.children.splice(0, this._configurationService.settings.i18n.locales.length);
        }
      }
    }
  }

  changeLanguage(lang) {
    // auto-reloads on language change
    this._configurationService.currentLocale = (ISO6391.getCode(lang));
  }
}

