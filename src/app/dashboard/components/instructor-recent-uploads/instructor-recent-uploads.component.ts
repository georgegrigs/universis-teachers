import { Component, OnInit } from '@angular/core';
import { CoursesService } from './../../../courses/services/courses.service';

/**
 *
 * InstructorRecentUploadsComponent
 *
 * Fetches and displays information about the user's last upload actions
 *
 */

@Component({
  selector: 'app-instructor-recent-uploads',
  templateUrl: './instructor-recent-uploads.component.html'
})
export class InstructorRecentUploadsComponent implements OnInit {
  public actions = [];
  public isLoading = true;

  constructor(private coursesService: CoursesService) { }

  ngOnInit() {
    this.loadData();
  }

  /**
   *
   * Fetches exams data and maps them to UploadAction objects.
   *
   */
  async loadData(): Promise<void> {
    this.coursesService.getUploadHistoryRecent()
      .then((res) => {
        this.actions = res;
        this.isLoading = false;
      })
      .catch(() => {
        this.isLoading = false;
      });
  }

  getUrl(courseExamDocument: any): Array<any> {
    if (courseExamDocument.object.classes.length > 0) {

      const latestClass = courseExamDocument.object.classes.sort((a: any, b: any) => {
        return a.courseClass.period > b.courseClass.period ? -1 : 1;
      })[0];

      return ([
        '/courses',
        courseExamDocument.object.course.id,
        courseExamDocument.object.year.id,
        latestClass.courseClass.period,
        'exams',
        courseExamDocument.object.id
      ]);
    } else {
      return [];
    }
  }
}
