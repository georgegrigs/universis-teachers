import { Component, OnInit } from '@angular/core';
import {ProfileService} from '../../services/profile.service';
import {ErrorService} from '@universis/common';

@Component({
  selector: 'app-profile-preview',
  templateUrl: './profile-preview.component.html',
  styleUrls: ['./profile-preview.component.scss']
})
export class ProfilePreviewComponent implements OnInit {
  public instructor: any;
  public isLoading = true;   // Only if data is loaded

  constructor( private profileService: ProfileService
    , private _errorService: ErrorService) { }

  ngOnInit() {

    this.profileService.getInstructor().then(res =>{
      this.instructor = res; // Load data
      this.isLoading = false;
    }).catch( err => {
      // use error service navigate to error method
      return this._errorService.navigateToError(err);
    });

}

}
