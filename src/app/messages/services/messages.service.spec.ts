import { TestBed } from '@angular/core/testing';
import { MessagesService } from './messages.service';
import {HttpClientTestingModule, TestRequest} from "@angular/common/http/testing";
import { ApiTestingModule, ApiTestingController } from '@universis/common/testing';
import {MostModule} from '@themost/angular';


describe('MessagesService', () => {
  let mockApi: ApiTestingController;
  beforeEach(async () => {
    TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
      MostModule.forRoot( {
        base: '/',
        options: {
          useMediaTypeExtensions: false
        }
      }),
      ApiTestingModule.forRoot()
    ],
    providers: [
      MessagesService
    ],
    declarations: []
  });
    mockApi = TestBed.get(ApiTestingController);
  });

  it('should be created', () => {
    const service: MessagesService = TestBed.get(MessagesService);
    expect(service).toBeTruthy();
  });
});
